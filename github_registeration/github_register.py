#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

# Extract the Firefox driver and place in the same folder as this file.

browser = webdriver.Firefox()

# Github new account registration website's URL

browser.get('https://github.com/join')


# In GUI, the email, name would already be stored from sign-up module
# Then, user only needs to enter username and password

def github_account(username, email, password):


    user_name = browser.find_element_by_id('user_login')
    user_name.send_keys(username)

    user_email = browser.find_element_by_id('user_email')
    user_email.send_keys(email)

    user_password = browser.find_element_by_id('user_password')
    user_password.send_keys(password)

    submit = browser.find_element_by_id('signup_button')
    submit.click()

    browser.quit()
    

if __name__ == '__main__':

    username = input('Enter username: ')
    email = input('Enter email: ')
    password = input('Enter password: ')
    github_account(username, email, password)
    print ('Congrats! Your new Github account has been created.')
    print ('Check your mail to verify it.')
