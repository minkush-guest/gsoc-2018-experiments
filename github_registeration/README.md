## Github Registration Script

This is a Python program which uses Selenium Web-driver to automate the process of new account registration on [Github](https://github.com/) for a user.

## Setup

* Tested on Debian Stretch, Python =>3.5, Firefox

* Install `Selenium` API using pip:

        `pip install selenium`

* Download browser driver (geckodriver for Firefox) from [here](https://github.com/mozilla/geckodriver/releases) and extract it in the same directory as the script.

* Run `github_register.py`

## Tests

* Pytest has been used to write test scripts for the program.

	Install using pip with:

		`pip install pytest`

* For running tests:

		`pytest test_github_register.py`