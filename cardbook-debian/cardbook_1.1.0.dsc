Format: 3.0 (native)
Source: cardbook
Binary: cardbook
Architecture: any
Version: 1.1.0
Maintainer: Minkush Jain <minkushjain@gmail.com>
Homepage: https://gitlab.com/CardBook/CardBook
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9)
Package-List:
 cardbook deb unknown optional arch=any
Checksums-Sha1:
 5f7fdcc61ba7926ce541bd4c956f8d4dfcb24f4a 461868 cardbook_1.1.0.tar.xz
Checksums-Sha256:
 605e0e72f155ed6535e3512d5915ee359d6e529fed17b5e1871382b4ebff0e74 461868 cardbook_1.1.0.tar.xz
Files:
 7ca124a068f5c9c7c45ef25811abcf6c 461868 cardbook_1.1.0.tar.xz
