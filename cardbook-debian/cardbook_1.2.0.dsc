Format: 3.0 (native)
Source: cardbook
Binary: cardbook
Architecture: any
Version: 1.2.0
Maintainer: Minkush Jain <minkushjain@gmail.com>
Homepage: https://gitlab.com/CardBook/CardBook
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9)
Package-List:
 cardbook deb unknown medium arch=any
Checksums-Sha1:
 84b84d381526cb95842c1b7ca0b8619685c6e8c1 460908 cardbook_1.2.0.tar.xz
Checksums-Sha256:
 b570d1a86749e2cb11845e281ac4cea36c08ab1d1b7eaf7414febcd0a67f0776 460908 cardbook_1.2.0.tar.xz
Files:
 5ac979dfd4a09f570bac204aa03d0ec4 460908 cardbook_1.2.0.tar.xz
