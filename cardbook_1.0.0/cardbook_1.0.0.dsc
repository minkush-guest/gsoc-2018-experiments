Format: 3.0 (native)
Source: cardbook
Binary: cardbook
Architecture: any
Version: 1.0.0
Maintainer: Minkush Jain <minkushjain@gmail.com>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9)
Package-List:
 cardbook deb unknown optional arch=any
Checksums-Sha1:
 761b3adf3177e51d343ae0950a38dbe57fd10db8 7204 cardbook_1.0.0.tar.xz
Checksums-Sha256:
 08581cae029aa7b9f93d8f6407c258cd3c9a2ad4a410136c72c26356fbc416ac 7204 cardbook_1.0.0.tar.xz
Files:
 c3533584eefa27b4babc5d916390301a 7204 cardbook_1.0.0.tar.xz
