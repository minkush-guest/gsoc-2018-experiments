## HTTP Post Salsa Registration

This is a program to register new Salsa script using HTTP Post method.

The script uses `requests` library to send HTTP requests and extract data from website and `BeautifulSoup` to parse and navigate HTML and XML data.

Run the script, enter your details to create new account on Salsa. Check your mail for further instructions.

### Requirements

* Tested on Python =>3.5

* `Requests` library

    Install using pip:
    
        `pip install requests`
    
* 'Beautiful Soup` linrary

    Install using system package manager (Debian/Linux):
    
        `apt-get install python3-bs4`
        
    Install using pip:
        
        `pip install beautifulsoup4`
        
* Run `salsa_http_post.py` and enter your details.