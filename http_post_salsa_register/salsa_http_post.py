#!/usr/bin/python
# -*- coding: utf-8 -*-
# using Requests and Beautiful Soup to automate salsa account registration

import requests
from bs4 import BeautifulSoup
import getpass


def register_account(
    username,
    full_name,
    email_address,
    user_password,
    ):

  # Submitting data

    url = 'https://signup.salsa.debian.org/register/guest/'
    features = 'html.parser'

    parse_link = requests.get(url)

  # Scraping HTML data

    soup = BeautifulSoup(parse_link.content)
    form = soup.find('form')
    fields = form.find('input')
    token = fields['value']

  # Defining data values

    data = [
        ('csrf_token', token),
        ('username_prefix', username),
        ('name', full_name),
        ('email', email_address),
        ('password', user_password),
        ('confirm', user_password),
        ]

  # Posting data and passing cookies

    response = \
        requests.post('https://signup.salsa.debian.org/register/guest/'
                      , cookies=parse_link.cookies, data=data)


if __name__ == '__main__':

    print('Welcome to GSoC 2018')
    print('Project: Wizard/GUI Helping Students get Started\n')
    print('This is a program to automate the process of new account creation on Salsa using HTTP Post.\n')
    username = input('Enter you username: ')
    full_name = input('Enter you name: ')
    email_address = input('Enter you email: ')

    # Input password:

    while True:
      try:
        user_password = getpass.getpass()
      except Exception as err:
        print('ERROR:', err)
      try:
        confirm_password = getpass.getpass(prompt='Confirm password: ')
      except Exception as err:
        print('ERROR:', err)

      # Checking for password mismatch

      if user_password == confirm_password:
        break
      else:
        print("Passwords don't match. Enter again\n")
    
    register_account(username, full_name, email_address, user_password)
    print("Congrats! Your new Salsa account has been created. Check your mail for further instructions")
