#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from os import *
import pygount
import file_extensions
from file_extensions import *


def source_code_scanner(analyse_location):

    file_path = []
    code_line = []

    for (dirpath, dirnames, filenames) in walk(analyse_location):

        for files in filenames:

            location = os.path.join(dirpath, files)
            file_path.append(location)

    total_lines = 0

    for file_type in file_extensions.keys():

        sum_of_lines = 0
        for path in range(len(file_path)):

            if isinstance(file_extensions[file_type], list) == True:

                for extension in file_extensions[file_type]:

                    if extension in str(file_path[path]):

                        analysis = \
                            pygount.source_analysis(file_path[path],
                                'pygount')
                        sum_of_lines += analysis.code
            elif file_extensions[file_type] in str(file_path[path]):

                analysis = pygount.source_analysis(file_path[path],
                        'pygount')
                sum_of_lines += analysis.code

        total_lines = total_lines + sum_of_lines
        code_line.append(sum_of_lines)

        if sum_of_lines != 0:

            print ('%s: %s line(s) of code' % (file_type, sum_of_lines))

    print ('\n')
    file_types = []

    for languages in file_extensions.keys():

        file_types.append(languages)

    language_percent = {}

    # i is a variable for loop

    for i in range(len(code_line)):

        percentage = code_line[i] / total_lines * 100
        language_percent[file_types[i]] = percentage

        if percentage != 0:

            print('Percentage of %s : %.3f %s ' % (file_types[i],
                    percentage, '%'))

    popular_lang = max(language_percent, key=language_percent.get)
    print('''
You are most proficient in %s language 
''' % popular_lang)


if __name__ == '__main__':

    project_directory = input("Enter the script's directory's path: ")
    source_code_scanner(project_directory)
