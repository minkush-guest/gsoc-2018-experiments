## Source Code Scanner and Analyser

This is a program to analyse scripts and code in user's project directory to find which Programming language they prefer.

The output is shown as total number of lines and percentage of each language present.

### Setup

* Tested on Python =>3.5

* Install `Pygount` library using pip:

		`pip install pygount`
		
* Run `source_scanner.py` script.