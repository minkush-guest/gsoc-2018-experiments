#!/usr/bin/python
# -*- coding: utf-8 -*-

# File extensions for various programming languages.

file_extensions = {
    
    "JavaScript": ".js",
    "Python": ".py",
    "Java": ".java",
    "C": ".c",
    "C++": ".cpp",
    "Markdown": ".md",
    "Objective-C": [
        ".m", 
        ".mm"
        ],
    "Shell": [
        ".sh", 
        ".bash"
        ],
    "GLSL": [
        ".vert", 
        ".frag", 
        ".vsh", 
        ".fsh", 
        ".glsl"
        ],
    "ActionScript": ".as",
    "CMake": ".cmake",
    "CoffeeScript": [
        "._coffee", 
        ".cson", 
        ".iced"
        ],
    "Ruby": [
        ".rb", 
        ".rhtml"
        ],
    "XML": [
        ".xml", 
        ".rss", 
        ".svg"
        ],
    "SQL": ".sql",
    "Scala": ".scala",
    "Rust": ".rs",
    "Perl": ".pl",
    "HTML": [
        ".html", 
        ".htm", 
        ".xtml", 
        ".jhtml"
        ],
    "CSS": ".css",
    "RSS": ".rss",
    "Atom": ".atom",
    "PHP": [
        ".php", 
        ".php4", 
        ".php3", 
        ".phtml"
        ]

    }