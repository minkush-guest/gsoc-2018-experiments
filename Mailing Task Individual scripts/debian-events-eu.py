#debian-events-eu
import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

print("This is a program to automate the process of subscription to the mailing list.\n")

#Submitting user email on the website:
with requests.Session() as c:
    url="https://lists.debian.org/cgi-bin/subscribe.pl"
    email=input("Enter you E-mail: ")    #enter user's email
    c.get(url)
    login_data={'user_email':email, 'list':'debian-events-eu', 'action':'Subscribe'}
    c.post(url,data=login_data,stream=True)
    print("\nCheck your mail for further instructions!")

#confirming subscription by replying to the mail:
    
a=input("\nEnter the code of the subject you recieved in your mail: ")
b="CONFIRM "+a       # Subject
fromaddr = "gsocdebian@gmail.com"  # Enter sender's email address
toaddr = "debian-events-eu-request@lists.debian.org"
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = b
 
body = "Subscribe"
msg.attach(MIMEText(body, 'plain'))
 
server = smtplib.SMTP('smtp.gmail.com', 587)   #connects to server using port 587
server.starttls()
server.login(fromaddr, "testuser")    #Enter password of sender's email in " "
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
print("\nCongrats, your email has been successfully registered in debian-events-eu mailing list!")
k=input("\nPress any key to exit")
