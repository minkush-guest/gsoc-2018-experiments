import pytest
import string
import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from salsa_account import *


class TestRegisteration:

    def test_create_salsa_account(self):

        random_id = ''.join([random.choice(string.ascii_letters
            + string.digits) for n in range(8, 18)])

        random_string = random_id 

        user_email = random_string + "@sdf.com"
        username = random_id
        create_salsa_account(username, "FullName", user_email,"testpassword")
        WebDriverWait(browser, 10)
        text = "Your requested user account "+username+"-guest was successfully created."
        assert browser.find_element_by_tag_name("p").text == text
