## IRC Setup

This is an Python IRC Bot script to help a person new to IRC get started.

It implements the following:

* Registers new nickname for a user
* Checks for already registered nicknames and emails
* Lists the available Debian channels
* Prompts the user to join any channel
* Implements ping-pong protocol to keep the server live

### Usage

* Tested on Python =>3.5

* No additional modules required.

* Run `irc_nick.py` from terminal and follow the instructions.