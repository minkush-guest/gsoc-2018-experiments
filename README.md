## [Wizard/GUI helping new interns get started](https://wiki.debian.org/NewContributorWizard) 

Final Work Product : https://wiki.debian.org/MinkushJain/WorkProduct

Summary of Final Work (Blog): http://minkush.me/gsoc-2018-final-report/

My Wiki: https://wiki.debian.org/MinkushJain/


### Files/Directories


`irc_setup`           IRC script for registering new nickname, displaying channels and joining them

`mail_subscribtion`     Script to subscribe to various Debian mailing lists and confirmation by sending code.

`salsa_registration`      Script to register new Salsa account and its Tests script

`github_registration`   Script to create new Github account and its Tests script

`geckodriver-v0.20.1-linux64.tar`   Firefox geckodriver required with Salsa and Github scripts

`http_post_salsa_register` Salsa registration script using HTTP Post

`thunderbird_config` Script that kills thunderbird process, if running and then edit its Preferences and settings.

`source_code_scanner`   Analyses source code and displays the preferable language

`extract_data_email`    Extracts telephone numbers from emails

`filter_email`  Email filtering script

### License

THis project is under GNU GPL3 License

## Contributor

Minkush Jain <minkushjain@gmail.com>
