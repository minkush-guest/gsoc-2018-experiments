#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, os.path
import psutil

# Ending Thunderbird process
PROCNAME = 'thunderbird'

for proc in psutil.process_iter():
    
    if proc.name() == PROCNAME:
        
        proc.kill()


# Editing Thunderbird's config
def edit_config(username):
    
    name_of_file = 'user.js'
    
    # new Preferences
    toFile = \
        '''user_pref("mail.attach_vcard", true);
    user_pref("mail.biff.play_sound", false);
    user_pref("mail.biff.show_alert", false);
    user_pref("mail.collect_email_address_outgoing", true);
    user_pref("mail.default_html_action", 0);
    user_pref("mail.html_compose", false);
    user_pref("mailnews.reply_header_type", 2);
    user_pref("mailnews.reply_on_top", 1);
    user_pref("mailnews.reply_with_extra_lines", 1);'''
    
    
    # finding path
    file_path = []

    for r, d, f in os.walk("/home/%s/.thunderbird/" % username):
        
        for files in f:
            
            if files == "prefs.js":
              path = os.path.join(r, files)
                file_path.append(path)

    # Editing the path

    if response == "Y":


        for i in range(len(file_path)):
        
            save_path = file_path[i][0:-9]

            completeName = os.path.join(save_path, name_of_file)

            file1 = open(completeName, 'w')
            file1.write(toFile)
            file1.close()

    elif response == "N":

        profile = input("Enter the name of the\
        profile which you want to edit: ")
        
        for i in range(len(file_path)):
            
            if profile in file_path[i]:

                save_path = file_path[i][0:-9]

                completeName = os.path.join(save_path, name_of_file)

                file1 = open(completeName, 'w')
                file1.write(toFile)
                file1.close()

            else:

                print("Profile not found")
                break
            
    else:
        
        print("Wrong Response")


if __name__ == '__main__':

    print('Program to close Thunderbird application\
    and edit its configuration settings\n')
    
    username = input("Enter your PC's user name: ")
    response = input("Do you want to edit configuration for all\
    your profiles (y/n) ").upper()
    
    edit_config(username)
