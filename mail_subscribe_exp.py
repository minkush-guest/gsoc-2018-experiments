import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

print("This is a program to automate the process of subscription to the mailing list.\n")

#created codes between S1----S10 corresponding to each mailing list. In GUI, S1---S10 would be the name of ten "Subscribe" buttons.

list_code=input("Enter mailing list code between S1 and S10: ")   #variable list_code will contain the mailing list code.

if list_code=="S1":
    listname = "debian-outreach"

elif list_code=="S2":
    listname = "debian-news"
    
elif list_code=="S3":
    listname = "debian-announce"

elif list_code=="S4":
    listname = "debconf-announce"    

elif list_code=="S5":
    listname = "debian-devel-announce"

elif list_code=="S6":
    listname = "debian-events-apac"

elif list_code=="S7":
    listname = "debian-events-eu"

elif list_code=="S8":
    listname = "debian-events-na"

elif list_code=="S9":
    listname = "debian-events-ha"

elif list_code=="S10":
    listname = "debian-dug-in"
  
#Submitting user email on the website:
    
with requests.Session() as c:
    
    url="https://lists.debian.org/cgi-bin/subscribe.pl"
    email=input("Enter you E-mail: ")    #enter user's email here
    c.get(url)
    login_data={'user_email':email, 'list':listname, 'action':'Subscribe'}
    c.post(url,data=login_data,stream=True)
    print("\nCheck your mail for further instructions!")

#confirming subscription by replying to the mail:
    
reply_code = input("\nEnter the code of the subject you recieved in your mail: ")
reply_subject = "CONFIRM "+reply_code       # Final Subject of reply mail
fromaddr = "gsocdebian@gmail.com"  #sender's email address (don't change)
toaddr = listname+"-request@lists.debian.org"
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = reply_subject
 
body = "Subscribe"
msg.attach(MIMEText(body, 'plain'))
 
server = smtplib.SMTP('smtp.gmail.com', 587)   #connects to server using port 587
server.starttls()
server.login(fromaddr, "testuser")   
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
print("\nCongrats, your email has been successfully registered in %s mailing list!" %listname)
k=input("\nPress any key to exit")
