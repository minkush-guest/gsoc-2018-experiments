## Thunderbird Configuration Script

This is a Python program to edit Thunderbird configuration for a user to make it more productive.

Features:

* Kills Thunderbird process if it is running in background.

* Feature to edit settings for all Mozilla profiles or specific one.

* Edits configuration to implement the following:

  * Default plain text mode in outgoing mails
   
  * Top-posting configuration
   
  * Mute notifications and sound alert for incoming mails

  * Allow v-card attachment
   
  * Collect outgoing email addresses

    and many more...

## Setup

* Tested on Python =>3.5

* Install `psutil` module using pip:

        `pip install psutil`

* Run `thunderbird_config.py` and enter your system's username.