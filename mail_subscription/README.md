## Mailing Lists Subscription Program

This is a Python script to automate the process of subscription to Debian Mailing lists.

Features:

* Subscribes to mailing list entered by the user.
* Automated confirmation process by replying user-specific code via email

### Setup

* Tested on Python =>3.5

* `Requests` library in python

	Install using pip with:

		`pip install requests`

* Run `mail_subscribe.py`