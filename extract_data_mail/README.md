## Extracting Phone Numbers from Emails

This is a Python program which extracts all phone numbers from a user's email.

Features:

* Examines all emails and identifies phone numbers in them.
* Converts the tel-numbers in International Standard Format.
* Finds the Location, Timezone and Carrier name for the phone numbers.
* Saves all the data along with sender's name in a text file.

### Setup

* Tested on Python =>3.5

* 'Phonenumbers' library in Python

	Install using pip with:

		`pip install phonenumbers`

* Make sure your email has IMAP access enabled in settings. Follow the instructions [here](https://support.google.com/mail/answer/7126229?hl=en) to enable it.

* Run `extract_data_email.py` and enter your details.

