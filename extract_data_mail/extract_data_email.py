#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import email
import imaplib
import mailbox
import phonenumbers
import getpass
from phonenumbers import geocoder
from phonenumbers import carrier
from phonenumbers import timezone


def extract_mail_data(email_account, password):

    # Connecting to gmail server using IMAP

    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(email_account, password)
    mail.list()
    mail.select('inbox')

    # ALL or UNSEEN mail

    (result, data) = mail.uid('search', None, 'ALL')
    data_length = len(data[0].split())

    # Fetching emails, sender, receiver, message, date-time

    for email_sequence in range(data_length):

        latest_email_uid = data[0].split()[email_sequence]
        (result, email_data) = mail.uid('fetch', latest_email_uid,
                '(RFC822)')
        raw_email = email_data[0][1]
        raw_email_string = raw_email.decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        # Header Details

        date_tuple = email.utils.parsedate_tz(email_message['Date'])

        if date_tuple:
            local_date = \
                datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            local_message_date = '%s' \
                % str(local_date.strftime('%a, %d %b %Y %H:%M:%S'))

        email_sender = email.header.decode_header(email_message['From'])
        email_receiver = email.header.decode_header(email_message['To'])
        email_sub = email.header.decode_header(email_message['Subject'])

        email_from = str(email.header.make_header(email_sender))
        email_to = str(email.header.make_header(email_receiver))
        subject = str(email.header.make_header(email_sub))

        # Body details

        for part in email_message.walk():
            if part.get_content_type() == 'text/plain':
                body = part.get_payload(decode=True)
                file_name = 'email_' + str(email_sequence) + '.txt'
                output_file = open(file_name, 'w')

                text = body.decode('utf-8')

                # Searching for phone numbers from email's body

                for match in phonenumbers.PhoneNumberMatcher(text,
                        None):
                    sender = 'From: %s' % email_from
                    print(sender)
                    output_file.write(str(sender) + '\n')

                    # Converting phone numbers to International format

                    phone_no = phonenumbers.format_number(match.number,
                            phonenumbers.PhoneNumberFormat.E164)
                    print(phone_no)
                    output_file.write(phone_no + '\n')

                    # Location of phone numbers

                    ph_location = \
                        geocoder.description_for_number(match.number,
                            'en')
                    print(ph_location)
                    output_file.write(ph_location + '\n')

                    # Carrier's name of the number

                    ph_carrier = carrier.name_for_number(match.number,
                            'en')
                    print(ph_carrier)
                    output_file.write(ph_carrier + '\n')

                    # Timezone for phone numbers

                    ph_timezone = \
                        timezone.time_zones_for_number(match.number)
                    print(ph_timezone)
                    output_file.write(str(ph_timezone) + '\n' + '\n')

                    print('\n')

                output_file.close()

            else:
                print('Not found')


# Stand-alone script condition

if __name__ == '__main__':

    user_email = input('Enter your email: ')
    password = getpass.getpass()
    extract_mail_data(user_email, password)
