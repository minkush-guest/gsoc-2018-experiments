#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import socket
import string
import time
import getpass


def socket_conn():

    global s, HOST, PORT
    HOST = 'irc.freenode.net'
    PORT = 6667
    
    s = socket.socket()
    s.connect((HOST, PORT))
    
    
def server_conn(REALNAME, NICK):

    IDENT = irc_nick

    s.send(bytes('NICK %s\r\n' % NICK, 'UTF-8'))
    s.send(bytes('USER %s %s bla :%s\r\n' % (IDENT, HOST, REALNAME),
           'UTF-8'))


def nick_register(
    NICK,
    mail,
    password,
    ):
    
    # Invalid nickname error
    error1 = False
    # Invalid email error
    error2 = False
    
    global irc_nick, email
    
    server_conn(user_name, NICK)

    s.send(str.encode('PRIVMSG NickServ :REGISTER %s %s\r\n'
           % (password, mail)))

    
    while 1:
        
        data = s.recv(1024).decode('UTF-8', errors="ignore")
        print(data)

        if "already registered" in data:
            error1 = True
            break

        elif "not a valid email address" in data:
            error2 = True
            break

        elif "activation instructions" in data:
            break


    s.close()
    

    if error1 == True:
        
        print("\nThe IRC nickname you have entered is already registered.\n")
        
        irc_nick = input("Enter another IRC nickname: \n")
        socket_conn()
        nick_register(irc_nick, email, password)


    elif error2 == True:
        
        print("\nThe email you have entered is not a valid.\n")
        
        email = input("Enter another email address: \n")
        socket_conn()
        nick_register(irc_nick, email, password)
    

    print("\nYour nickname has been successfully registered\
    on the server. Check your email for further instructions!\n\n")
    
    # Waiting server for 5s for user.
    time.sleep(5)
    
    
def list_channel():

    socket_conn()
    server_conn(user_name,irc_nick)
    s.send(bytes('PRIVMSG alis :LIST #debian* -min 30\r\n', 'UTF-8'))    
    
    global bufferdata
    bufferdata = ""
    
    while 1:

        connection_str = ":%s MODE %s :+Ri" %(irc_nick, irc_nick)     

        data = s.recv(1024).decode('UTF-8', errors="ignore")

        bufferdata = bufferdata + '%s\n' %(data)


        if "End of output" in data:
            break

    s.close()

    # Not display data before channels list
    
    if connection_str in bufferdata:
        l = bufferdata.index(connection_str)

        print(bufferdata[l+27:])


def join_channel(channel):

    readbuffer = ''
    
    socket_conn()
    server_conn(user_name,irc_nick)
    
    time.sleep(10)
    
    # Verifying nickname on the server.
    s.send(bytes('PRIVMSG NickServ :IDENTIFY %s\r\n' %(password), 'UTF-8'))
    
    # Waiting for server to respond.
    time.sleep(10)
    
    # Joining channel
    s.send(bytes('JOIN %s\r\n' %(channel), 'UTF-8'))
    

    while 1:

        data = s.recv(1024).decode('UTF-8', errors="ignore")
        readbuffer = readbuffer + data
        temp = str.split(readbuffer, '\n')
        readbuffer = temp.pop()

        for line in temp:
            line = str.rstrip(line)
            line = str.split(line)
            
            # ping pong protocol
            if line[0] == 'PING':
                s.send(bytes('PONG %s\r\n' % line[1], 'UTF-8'))

        print(data)   


if __name__ == '__main__':
    
    socket_conn()
    
    print('This Program will automate IRC Nickname Registration,\
    Displaying List of Channels and Joining them.\n')
    
    user_name = input('Enter your name: ')
    irc_nick = input('Enter the IRC nickname you want to register: ')
    email = input('Enter your email: ')
    password = getpass.getpass('Enter password for your nickname: ')
    
    socket_conn()
    nick_register(irc_nick, email, password)
    
    display_channels = input("Do you want to display the list of available 'debian' channels? (Y/N) ").upper()
    
    if display_channels == "Y":
        
        list_channel()
        irc_channel = input("Enter the channel you want to join: ")
        join_channel(irc_channel)

    else:
        irc_channel = input("Enter the channel you want to join: ")
        join_channel(irc_channel)
