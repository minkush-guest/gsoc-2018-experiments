# Testing script to find path inside a system for Thunderbird script


# Testing script using os library.

import os

username = input("Enter your system's username")

for roots,dirs,files in os.walk("/home/%s/.thunderbird/" % username):

    for files in f:
         if files == "prefs.js":
              a = os.path.join(r,files)
              print (a[0:-9])



# Testing script using subprocess library.

import subprocess

username = input("Enter your system's username")

command = "find"
directory = "/home/%s/.thunderbird/" % username
flag = "-iname"
file = "prefs.js"

args = [command, directory, flag, file]
process = subprocess.run(args, stdout=subprocess.PIPE)
path = process.stdout.decode().strip("\n")
print(path)
