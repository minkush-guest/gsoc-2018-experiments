# Webbrowser library to open web page inside GUI

# Open Jabber Registration Page to create JID inside GUI.

import webbrowser
print("Fill the form on the webpage to create your Jabber account and get started with using XMPP")
webbrowser.open('https://jabber.hot-chilli.net/forms/create/', new=0, autoraise=True)


# Open Thunderbird Download page inside GUI

import webbrowser
print("Download Thunderbird for Debian from here")
webbrowser.open('https://www.thunderbird.net/en-US/', new=0, autoraise=True)


# Open Pidgin Download Page inside GUI.

import webbrowser
print("Download Pidgin for XMPP chat from here")
webbrowser.open('https://jabber.hot-chilli.net/forms/create/', new=0, autoraise=True)
